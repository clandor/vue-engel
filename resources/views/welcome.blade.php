<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>

        @vite('resources/sass/app.scss')
    </head>
    
    <body class="antialiased">
        <div id="app">
            {{-- <div class="container">
                <div class="row info-container">
                    <div class="peoplesContainer col-md-3">
                        <div v-if="peoples" class="col-12">
                            <template v-for="(people, peopleIndex) of peoples.results">
                                <example-component @info-people="getInfoPeople" :people="people"></example-component>
                            </template>
                        </div>

                        <span v-else>Cargando...</span>
                    </div>

                    <div class="col-md-9 details">
                        <span v-if="!infoPeople">Cargando...</span>

                        <info-people v-else :details="infoPeople"></info-people>

                        <button-url v-if="peoples" :data="peoples.results"></button-url>
                    </div>
                </div>
            </div> --}}

            <div class="container">
                <div class="info-container row">
                    <peoples-component />
                </div>
            </div>

            {{-- <button-example @alert-message="showAlert" text="Engels"></button-example>
            <button-example @alert-message="showAlert" text="Clandor"></button-example>
            <button-example @alert-message="showAlert" text="Orlando"></button-example> --}}
        </div>
        
        @vite('resources/js/app.js')
    </body>
</html>
