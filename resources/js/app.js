import './bootstrap';
import { createApp } from 'vue';
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import { createStore } from 'vuex'


// Acá se inicializa el store
const store = createStore({
    state () {
      return {
        peoples: null,
        detailsPeople: null,
      }
    },
    mutations: {
      setPeoples (state, peoples) {
        state.peoples = peoples
      },
      setDetailPeople (state, details) {
        state.detailsPeople = details
      }
    }
})

// Acá se  inicializa app, es es quien posee toda la información
const app = createApp({
    data() {
        return {
            peoples: null,
            infoPeople: null,
        }
    },
    methods: {
        getUsers() {
            axios.get('https://swapi.dev/api/people/')
            .then((response) => {
                this.peoples = response.data
                this.infoPeople = response.data.results[0]

                this.$store.commit('setPeoples', response.data.results)
            });
        },
        getInfoPeople(url) {
            this.infoPeople = null

            axios.get(url)
            .then((response) => {
                this.infoPeople = response.data
            });
        },
        viewInfoPeople(info) {
            this.infoPeople = info
        }
    },
    mounted() {
        this.getUsers()
    }
});

// Acá se importan los componentes para que puedan ser usados
import ExampleComponent from './components/ExampleComponent.vue';
import buttonExample from './components/buttonExample.vue';
import infoPeople from './components/infoPeople.vue';
import peoplesComponent from './components/peoplesComponent.vue';
import buttonUrl from './components/buttonUrl.vue';

// Acá los componentes pasan a ser parte del app
app.component('example-component', ExampleComponent);
app.component('button-example', buttonExample);
app.component('info-people', infoPeople);
app.component('peoples-component', peoplesComponent);
app.component('button-url', buttonUrl);


// Acá hacemos uso de las librerías y montamos app
app.use(store)
app.use(ElementPlus)
app.mount('#app');

